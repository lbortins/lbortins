# Weekly priorities
:coffee: This is a place to keep a running record of my weekly top priorities. This helps me to manage my time more effectively each week.

:thought_balloon: Also posted in the [Switchboard team slack channel](https://gitlab.enterprise.slack.com/archives/C04DG7DR1LG) for visibility and discussion if needed.

:heart: Inspired by [Snippets](https://blog.idonethis.com/google-snippets-internal-tool/).

## 2025-03-11

1. Q2 planning (issue)
1. Prep for user journey workshop (see plug in Infra Platforms all hands)
1. Smartsheet migration support
1. Comms for expanded secondary region support
1. Status page proposal doc (draft)
1. Discovery for automated maintenance at scale (doc coming soon)

## 2025-02-11

1. Closing out FY26 Q1 planning - scoping FedRamp for Gov / Comms MVC
1. Discovery for Status page in Switchboard (cool demo in thread)
1. Prep for onboarding `professional_teal_sparrow` to SSO for Switchboard
1. Refining Dedicated onboarding journey map (Figma)

## 2025-01-14

1. FY26 Q1 planning / epic prep (issue)
1. Support for elegant_bronze_hookworm geo migration (epic)
1. Support for FY25 Q4 customer onboardings
1. Discovery / direction for FY26 Q1 (customer comms MVC, SSO follow on work, configuration UI redesign)

## 2025-01-06

1. FY26 Q1 planning / epic prep
1. Support for elegant_bronze_hookworm geo migration (epic)
1. Storage utilization proposal (issue)
1. Misc MR / issue close out for FY25 Q4

## 2024-12-16

1. Prep for FY26 / FY26 Q1 planning
1. Support for elegant_bronze_hookworm geo migration (epic)
1. Storage utilization proposal (issue)
1. Finalizing SSO scope for Q4 (thread)
1. C6 BYOD migration (issue)

## 2024-11-26

1. Backlog restructuring/prep for FY26
1. Support for elegant_bronze_hookworm geo migration (epic)
1. Storage utilization proposal (issue)
1. C6 support - BYOD migration (issue)
1. Dedicated onboarding brainstorming with `@cbalane`

**De-prioritizing:** vision for status page (related to C8 support issue), Analytics implementation plan (issue), Tenant overview solution validation, non-blocking UX discussion (issues and MRs)

## 2024-11-19

1. Backlog restructuring/prep for FY26
1. Setting priorities for incoming Switchboard designer
1. Support for elegant_bronze_hookworm geo migration (epic)
1. Storage utilization proposal (issue)
1. C6 support - BYOD migration (issue)
1. Support for electric_salmon_narwhal escalation (channel)
1. Dedicated onboarding brainstorming with `@cbalane`

**De-prioritizing:** vision for status page (related to C8 support issue), Analytics implementation plan (issue), Tenant overview solution validation, non-blocking UX discussion (issues and MRs)

## 2024-11-12

1. Finalizing FY25 Q4 OKRs (issue)
1. Backlog restructuring/prep for FY26
1. Prep for elegant_bronze_hookworm geo migration (epic)
1. Storage utilization proposal (issue)
1. C6 support - BYOD migration (Issue)
1. End of Q3 clean up - doc updates, corrective actions etc.

**De-prioritizing:** vision for status page (related to C8 support issue), Analytics implementation plan (issue), Tenant overview solution validation, non-blocking UX discussion (issues and MRs)

## 2024-10-28

- Closing out FY25 Q4 planning (issue)
- New customer onboarding support (issue, issue)
- Prep for elegant_bronze_hookworm geo migration
- PHZ/OPL release comms
- C6 support - BYOD migration, Storage Utilization

**De-prioritizing:** vision for status page (related to C8 support issue), Backlog restructuring/prep for FY26, Analytics implementation plan (issue), Tenant overview solution validation, non-blocking UX discussion (issues and MRs)

## 2024-10-22

- Q4/FY26 planning (also formalizing requests for Design support)
- New customer onboarding support (one issue opened, another expected soon)
- PHZ/OPL release planning (PHZ copy, OPL copy/docs)
- C6 support - BYOD migration, Storage Utilization
- Short-term support plan for C8 (issue)

**De-prioritizing:** Backlog restructuring - prep for FY26, Analytics implementation plan (issue), Tenant overview solution validation, Field (Sales/CSM/Support) enablement (issue), non-blocking UX discussion (issues and MRs)

## 2024-10-08

- Q4/FY26 planning (also formalizing requests for Design support)
- Complete release prep for tenant overview page
- PHZ/OPL release planning (PHZ copy, OPL copy/docs)
- C6 support - BYOD migration, Storage Utilization
- (stretch) Backlog restructuring - prep for FY26

**De-prioritizing:** Analytics implementation plan (issue), Tenant overview solution validation, Field (Sales/CSM/Support) enablement (issue), non-blocking UX discussion (issues and MRs)

## 2024-09-30

- Q4 planning
- Backlog restructuring - prep for FY26
- Continued support/MRs for tenant overview page release (thread)
- Field (Sales/CSM/Support) enablement materials 
- (stretch) Analytics implementation plan (issue)

**De-prioritizing:** Tenant overview solution validation, non-blocking UX discussion (issues and MRs), OPL copy and docs updates (thread), PHZ copy updates

## 2024-09-17

- Roll out plan for tenant overview page (thread)
- Direction page update
- Field (Sales/CSM/Support) enablement materials 
- (stretch) Analytics implementation plan (issue)

**De-prioritizing:** Tenant overview solution validation, non-blocking UX discussion (issues and MRs), OPL copy and docs updates (thread), PHZ copy updates


## 2024-09-09

- Field (Sales/CSM/Support) enablement materials 
- Roll out plan for tenant overview page (thread)
- MR to update configuration doc page (related thread)
- Direction page update
- (stretch) Analytics implementation plan (issue)
- (stretch) Tenant overview solution validation

**De-prioritizing:** non-blocking UX discussion (issues and MRs), OPL copy and docs updates (thread), PHZ copy updates

## 2024-08-05

- Support new tenant onboardings (issue, issue)
- Support C2 hosted runner onboarding (issue)
- Discussion guide for tenant overview page
- Contribute to Q2 retro (issue) 
- Prepare for PTO coverage Aug 12-23 (issue)

## 2024-07-23

- Finalizing Q3 plan (issue) and OKRs (issue)
- Pro Services enablement call (issue)
- Q3 backlog refinement for maintenance epic items
- Support new tenant onboardings as needed (listed here)

## 2024-07-16

- Q3 planning (issue)
- Prep for Pro Services enablement call (issue)
- Support remaining discovery/definition for PHZ (epic) and tenant overview (epic)
- Support new tenant onboardings as needed (listed here)

## 2024-07-09

- Q3 planning (issue)
- Analytics instrumentation (issue)
- Prep for Pro Services enablement call (issue)
- Catch up from Holiday/PTO

## 2024-07-01

- Cleaning up internal dashboard epic - renamed to tenant overview (see update)
- Q3 planning (issue)
- Analytics instrumentation (mapping issue)
- Support for remaining Q2 epics - OPL, Hosted Runners, PHZ

## 2024-06-10

- Completing internal dashboard scoping - draft MVC proposal
- Analytics instrumentation (epic)
- Prep for Pro Services/Sales/CSM enablement call (issue)

## 2024-06-03

- Continuing to support Q2 customer facing epics (private link, hosted runners)
- Completing internal dashboard scoping - validating prioritization work with EA EMs this week
- Analytics instrumentation (epic)

## 2024-05-29

- Finalizing Q2 scope for internal tenant dashboards
- Reviewing open UX/Product questions for Q2 epics (private link, hosted runners)
- Support for C9 and C10 onboarding
- PTO catch up and carry over from last couple of weeks

## 2024-05-20

- Async workshop - prioritization for tenant dashboard
- Comms for configuration change log
- Q2 planning close out
- PTO prep

## 2024-05-14

- Async prep work for tenant dashboard scoping
- Comms for configuration change log
- Q2 planning close out
- PTO catch up

## 2024-05-07

- Q2 planning close out - refining OKRs
- Async prep work for tenant dashboard scoping
- Comms for configuration change log
- Contributing to Q1 Switchboard retro

## 2024-04-30

- Q2 planning and epic writing (issue)
- Beginning Q2 kick off with Divya and Amy (tenant dashboard issue)
- Working with Amy on first iteration of monthly delivery issue
- Understanding Hosted runners end to end flow (issue, FigJam)

## 2024-04-23

- 16.11 Release post retro - last item for RPM duties 
- April Performance Indicator (PI) review 
- Q2 planning and epic writing (issue)
- Q2 kick off preparation with Divya and Amy
- MR review catch up from last week

## 2024-04-09

- Support frontend work on Configuration Change Log (issue)
- Support BYOD Switchboard work (issue)
- Q2 planning and epic writing (issue)
- Release post manager duties for 16.11

## 2024-04-01

- Communications for email MVC release (issue)
- Support frontend work beginning on Configuration Change Log (issue)
- C9 onboarding - non-standard: will be changing primary admin (issue)
- Prep for solution validation on audit logs and email MVCs (issue)
- Beginning release post manager duties for 16.11
- Summit catch up (list)

## 2024-03-04

- Prep for solution validation on audit logs and email MVCs (issue)
- Review of sales enablement materials for Dedicated (epic)
- Discovery for hosted runners - Q2 Switchboard work (epic)
- Instrumentation epic - thinking through defining actions

## 2024-02-26

- Prep for solution validation on audit logs and email MVCs (issue)
- Review of sales enablement materials for Dedicated (epic)
- Discovery for hosted runners (Q2 Switchboard work)

## 2024-02-19

- Catch up from PTO
- Prep for solution validation on audit logs and email MVCs (issue)
- Finalizing designs for updates to configurations page (part of POC feedback epic)
- Feedback from C8 RE: SAML set up (issue)
- Discovery for hosted runners (Q2 Switchboard work)

## 2024-02-12

- Supporting internal onboarding (issue)
- Prep for solution validation on audit logs and email MVCs (issue)
- POC feedback issue clean up (epic)
- Discovery for hosted runners (Q2 Switchboard work)

## 2024-02-05

- Additional designs for email notifications MVC (issue)
- UX reviews on customer-facing MRs
- Prep for solution validation on audit logs and email MVCs (issue)
- POC feedback issue clean up (epic)
- Discovery for hosted runners (Q2 Switchboard work)

## 2024-01-29

- Supporting C8 onboarding (issue)
- Low-fidelity design for audit log MVC
- Low-fideltiy design for email notifications MVC
- POC feedback issue clean up (epic)

## 2024-01-22

- Finalize FY25 Q1 OKRs (issue)
- Low-fidelity design for audit log MVC
- Low-fideltiy design for email notifications MVC
- POC feedback issue clean up (epic)

## 2024-01-16

- Continue to flesh out Q1 epics (linked from planning issue)
- Deck for insights from customer call (to-do from UX/Prod/Eng sync)
- POC feedback issue clean up (epic)
- Shadowing as release post manager for 16.8

## 2024-01-08
- Prep for 1st SaaS Platforms PI review
- Close out Switchboard Direction page update (MR)
- Deck for insights from customer call (to-do from UX/Prod/Eng sync)
- POC feedback issue clean up (epic)
- FY25 Q1 Planning issue
- Shadowing as release post manager for 16.8
- Family and Friends Day Fri Jan 12 and public holiday Mon Jan 15

## 2024-01-03
- Catch up from winter break - focusing on slack/to-do's first
- Switchboard Direction page update
- FY25 Q1 planning issue

## 2023-12-25
:snowman: winter break!

## 2023-12-18
- Q1 planning - working with Amy to draft
- C7 onboarding
- Wrapping up work on issues from POC feedback
- Hand off issue for PTO :palm_tree: (2023-12-20 thru 2024-01-02)

## 2023-12-04
- Creating epic and issues based on customer feedback in last week's POC onboarding and today's SAML set up
- Q1 planning prep/direction page updates
- Participating in SaaS Platforms North Star workshop

## 2023-11-28
- Participating in SaaS Platforms North Star workshop
- Creating epic and issues based on customer feedback in last week's POC onboarding
- Q1 planning prep


